<img src="images/logo.png" width="220">

## What is FALL3D
FALL3D is a 3D model for atmospheric passive transport and deposition of 
particles, aerosols, and radionuclides.
The model, originally developed for inert volcanic particles (tephra), has a 
track record of 50+ publications on different model applications and code 
validation, as well as an ever-growing community of users worldwide, including 
academia, private, research, and several institutions tasked with operational 
forecast of volcanic ash clouds. 

## Documentation
For further information, please visit the [Wiki web site](https://gitlab.com/fall3d-distribution/v8.0/-/wikis/home).

## Screenshots
<img src="images/polar.png" width="450">

## Developers
FALL3D is developed and maintained by the **Barcelona Supercomputing Center (BSC)** and the **Istituto Nazionale di Geofisica e Vulcanologia (INGV)** 

1. [Arnau Folch](@afolch)
2. [Antonio Costa](@ant.costa)
3. [Giovanni Macedonio](@gmacedonio)
4. [Leonardo Mingari](@lmingari)

## License and copyright
Copyright (C) 2013, 2016, 2017, 2018, 2019 Arnau Folch, Antonio Costa and Giovanni Macedonio

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.

See the GNU General Public License [here](https://gitlab.com/fall3d-distribution/v8.0/blob/master/LICENSE)